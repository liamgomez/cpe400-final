//--------------------------------------------------------------------
// IMPL FILE:    ListLinked.cpp
//
// AUTHOR:       Connor Parkinson
//
// PURPOSE:      Contains implementation for the linked list ADT
//
//--------------------------------------------------------------------

//  Header Files  //////////////////////////////////////////////////////////////
#include "ListLinked.h"

//  ListNode Implementation  ///////////////////////////////////////////////////

template <typename DataType>
List<DataType>::ListNode::ListNode(const DataType& nodeData, ListNode* nextPtr)
  {
   // set dataItem to input nodeData
   dataItem = nodeData;

   // set next to input nextPtr
   next = nextPtr;
  }

//  List Implementation  ///////////////////////////////////////////////////////

template <typename DataType>
List<DataType>::List(int ignored)
  {
   // set head and cursor to null
   head = NULL;
   cursor = NULL;
  }

template <typename DataType>
List<DataType>::List(const List& other)
  {
   // if other list is empty
   if( other.isEmpty() )
     {
      // set head and cursor to null
      head = NULL;
      cursor = NULL;
     }

   // otherwise
   else
     {
      // initialize temporary node pointer for other list
      ListNode * otherTemp = other.head;

      // allocate head node and set data to other list head data
      head = new ListNode( otherTemp->dataItem, NULL );

      // set cursor to new head
      cursor = head;

      // move otherTemp to the next element to be copied
      otherTemp = otherTemp->next;

      // initialize temporary node pointer for this list
      ListNode * temp = head;

      // loop until end of other list
      while( otherTemp != NULL )
        {
         // allocate node for next and set data to other temp data
         temp->next = new ListNode( otherTemp->dataItem, NULL );

         // if other cursor points to the location of otherTemp
         if( other.cursor == otherTemp )
           {
            // set this cursor to this temp next
            cursor = temp->next;
           }

         // move temp nodes to the next element
         otherTemp = otherTemp->next;
         temp = temp->next;
        }
     }
  }

template <typename DataType>
List<DataType>& List<DataType>::operator=(const List& other)
  {
   // if other list is empty
   if( other.isEmpty() )
     {
      // set head and cursor to null
      head = NULL;
      cursor = NULL;
     }

   // otherwise
   else
     {
      // initialize temporary node pointer for other list
      ListNode * otherTemp = other.head;

      // allocate head node and set data to other list head data
      head = new ListNode( otherTemp->dataItem, NULL );

      // set cursor to new head
      cursor = head;

      // move otherTemp to the next element to be copied
      otherTemp = otherTemp->next;

      // initialize temporary node pointer for this list
      ListNode * temp = head;

      // loop until end of other list
      while( otherTemp != NULL )
        {
         // allocate node for next and set data to other temp data
         temp->next = new ListNode( otherTemp->dataItem, NULL );

         // if other cursor points to the location of otherTemp
         if( other.cursor == otherTemp )
           {
            // set this cursor to this temp next
            cursor = temp->next;
           }

         // move temp nodes to the next element
         otherTemp = otherTemp->next;
         temp = temp->next;
        }
     }

   // return this
   return *this;
  }

template <typename DataType>
List<DataType>::~List()
  {
   // initialize temporary node pointers
   ListNode * curTemp = head;
   ListNode * nextTemp = NULL;

   // loop until all nodes are deleted
   while( curTemp != NULL )
     {
      // set nextTemp to next node of curTemp
      nextTemp = curTemp->next;

      // delete node at curTemp
      delete curTemp;

      // set curTemp to node at nextTemp
      curTemp = nextTemp;
     }

   // set head and cursor to null
   head = NULL;
   cursor = NULL;
  }

template <typename DataType>
void List<DataType>::insert(const DataType& newDataItem) throw (logic_error)
  {
   // if list is empty
   if( isEmpty() )
     {
      // allocate node at head and set data to inData
      head = new ListNode( newDataItem, NULL );

      // assign cursor to head
      cursor = head;
     }

   // otherwise if list is not full
   else if( !isFull() )
     {
      // initialize temporary node pointer to next of cursor
      ListNode * nextTemp = cursor->next;

      // allocate new node for next of cursor, insert newDataItem and set new 
      //   node's next to old cursor next
      cursor->next = new ListNode( newDataItem, nextTemp );

      // move cursor to new node
      cursor = cursor->next;
     }
  }

template <typename DataType>
void List<DataType>::remove() throw (logic_error)
  {
   // if list is not empty
   if( !isEmpty() )
     {
      // initialize temporary node pointer to head
      ListNode * temp = head;

      // if cursor points to head
      if( cursor == head )
        {
         // set temp to next of head
         temp = head->next;

         // delete node at head
         delete head;

         // set head to node at temp
         head = temp;

         // reset cursor to head
         cursor = head;
        }

      // otherwise
      else
        {
         // move temp to node before cursor
         while( temp->next != cursor ) temp = temp->next;

         // set next of temp to next of cursor
         temp->next = cursor->next;

         // delete node at cursor
         delete cursor;

         // if last item in list was deleted, move cursor to beginning of list
         if( temp->next == NULL ) cursor = head;

         // otherwise, move cursor to the node that followed the deleted node
         else cursor = temp->next;
        }
     }
  }

template <typename DataType>
void List<DataType>::replace(const DataType& newDataItem) throw (logic_error)
  {
   // if list is not empty
   if( !isEmpty() )
     {
      // assign data of node at cursor to input data
      cursor->dataItem = newDataItem;
     }
  }

template <typename DataType>
void List<DataType>::clear()
  {
   // initialize temporary node pointers
   ListNode * curTemp = head;
   ListNode * nextTemp = NULL;

   // loop until all nodes are deleted
   while( curTemp != NULL )
     {
      // set nextTemp to next node of curTemp
      nextTemp = curTemp->next;

      // delete node at curTemp
      delete curTemp;

      // set curTemp to node at nextTemp
      curTemp = nextTemp;
     }

   // set head and cursor to null
   head = NULL;
   cursor = NULL;
  }

template <typename DataType>
bool List<DataType>::isEmpty() const
  {
   // if head equals null, return true
   if( head == NULL ) return true;

   // otherwise return false
   else return false;
  }

template <typename DataType>
bool List<DataType>::isFull() const
  {
   // assuming there is always more memory available, return false
   return false;
  }

template <typename DataType>

void List<DataType>::gotoBeginning() throw (logic_error)
  {
   // if list is not empty
   if( !isEmpty() )
     {
      // set cursor to head
      cursor = head;
     }
  }

template <typename DataType>
void List<DataType>::gotoEnd() throw (logic_error)
  {
   // if list is not empty
   if( !isEmpty() )
     {
      // initialize temporary node pointer
      ListNode * temp = NULL;

      // loop through list, from cursor on, with temp node until end is found
      for( temp = cursor; temp->next != NULL; temp = temp->next );

      // set cursor to temp node
      cursor = temp;
     }
  }

template <typename DataType>
bool List<DataType>::gotoNext() throw (logic_error)
  {
   // if list is not empty
   if( !isEmpty() )
     {
      // if cursor is not at end of list
      if( cursor->next != NULL )
        {
         // move cursor to next element
         cursor = cursor->next;

         // return success
         return true; 
        }
     }

   // return false
   return false;
  }

template <typename DataType>
bool List<DataType>::gotoPrior() throw (logic_error)
  {
   // if cursor is not at beginning of list
   if( cursor != head && !isEmpty() )
     {
      // initialize temporary node pointer
      ListNode * temp = NULL;

      // loop through list with temp node until node prior to cursor is reached
      for( temp = head; temp->next != cursor; temp = temp->next );

      // set cursor to temp node
      cursor = temp;
      // return success
      return true;
     }

   // otherwise return false
   else return false;
  }

template <typename DataType>
DataType List<DataType>::getCursor() const throw (logic_error)
  {
   // if list is not empty
   if( !isEmpty() )
     {
      // return data at cursor
      return cursor->dataItem;
     }
  }

// Programming exercise 2
template <typename DataType>
void List<DataType>::moveToBeginning () throw (logic_error)
  {
   // if list is not empty, and cursor is not at the beginning of the list
   if( !isEmpty() && cursor != head )
     {
      // initialize temp node pointer and counter
      ListNode * temp = NULL;
      DataType tempData;
      int counter = 0;

      // loop until temp node pointer is at node before cursor
      for( temp = head; temp->next != cursor; temp = temp->next, counter++ );

      // loop in reverse until no more swaps
      while( counter >= 0)
        {
         // swap next data value with data value at temp (shift left)
         tempData = temp->dataItem;
         temp->dataItem = temp->next->dataItem;
         temp->next->dataItem = tempData;

         // decrement counter
         counter--; 

         // reset temp to head
         temp = head;

         // interate through list, moving temp to next, counter # times
         //   (move temp to previous list item)
         for( int n = 0; n < counter; temp = temp->next, n++ );
        }

      // move cursor to beginning
      cursor = head;
     }
  }

// Programming exercise 3
template <typename DataType>
void List<DataType>::insertBefore(const DataType& newDataItem) throw (logic_error)
  {
   // if list is empty
   if( isEmpty() )
     {
      // allocate node at head and set data to inData
      head = new ListNode( newDataItem, NULL );

      // assign cursor to head
      cursor = head;
     }

   // otherwise if list is not full
   else if( !isFull() )
     {
      // initialize temp node pointer and counter
      ListNode * temp = cursor;
      int counter = 0;

      // loop until temp node pointer is at the last value in the list, count
      for( temp = cursor; temp->next != NULL; temp = temp->next, counter++ );

      // allocate node at end of list
      temp->next = new ListNode( '0', NULL );

      // loop in reverse until temp node pointer equals cursor
      while( temp != cursor)
        {
         // set next data value to data value at temp (shift right)
         temp->next->dataItem = temp->dataItem;

         // decrement counter
         counter--; 

         // reset temp to cursor
         temp = cursor;

         // interate through list, moving temp to next, counter # times
         //   (move temp to previous list item)
         for( int n = 0; n < counter; temp = temp->next, n++ );
        }

      // set next data value to data value at cursor
      cursor->next->dataItem = cursor->dataItem;

      // set data at cursor to input data
      cursor->dataItem = newDataItem;
     }
  }


