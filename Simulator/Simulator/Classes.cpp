#include "Classes.h"
#include <iostream>

int PACKET_COUNTER = 0;

//  Node  ////////////////////////////////////////////////////////
Node::Node()
{
   routingPath = new string[NUM_NODES];
}

int Node::doEvents( Packet * incoming, Packet ** outgoing )
{
   // Logging
   cout << "Node " << name << " doing events. Energy: " << energyLevel << endl;


   // If need to charge
   if (energyLevel < RECHARGE_LEVEL)
   {
      // Set charge state
      state = CHARGING;
   }

   // If charging
   if (state == CHARGING)
   {
      // Increase energy
      energyLevel += CHARGE_CYCLE;

      // Logging
      cout << "Node " << name << " charging" << endl;

      // If energy full
      if (energyLevel >= FULL_ENERGY)
      {
         // Change state to idling
         state = IDLING;

         // NOTE: We dont send energyLevel packet here to CC because route is likely outdated
      }
   }
   // Else, active
   else
   {
      // If packet received
      if (incoming != NULL)
      {
         // Lower energy levels for receiving
         energyLevel -= RECEIVE_COST;

         // If update packet from Command Center
         if (incoming->type == CC_UPDATE)
         {
            // update state
            state = stoi(incoming->information);

            // update routing path
            routingPath = incoming->routingPath;
         }

         // Else if energy request from Command Center
         else if (incoming->type == CC_ENERGY_REQUEST)
         {
            // Logging
            cout << "Node " << name << " recieved an energy info packet" << endl;

            // Prepare energyLevel packet
            *outgoing = new Packet();
            (*outgoing)->type = NODE_INFORMATION_PACKET;
            (*outgoing)->energyLevels[0] = energyLevel;
            for (int i = 0; i < NUM_NODES; i++)
            {
               (*outgoing)->routingPath[i] = routingPath[i];
            }
         }

         // Else if packet from node
         else if (incoming->type == NODE_INFORMATION_PACKET)
         {
            // Logging
            cout << "Node " << name << " recieved node info packet [ " << incoming->ID << " ]" << endl;

            // Add energy information of this node
            incoming->energyLevels[incoming->nextDestinationIndex] = energyLevel;

            // increment destination index
            incoming->nextDestinationIndex++;

            // Logging, avoid going past array bounds when testing
            if (incoming->nextDestinationIndex < 20)
            {
               cout << "Node " << name << " relayed the packet to node " << incoming->routingPath[incoming->nextDestinationIndex] << endl;

               // Set outgoing packet
               *outgoing = incoming;
            }
            else
            {
               cout << "Node " << name << " failed to relay packet: end of routing path reached" << endl;
            }
         }
      }

      // If not already sending a packet, and has valid route
      if (*outgoing == NULL && routingPath[0] == name)
      {
         // Generate packet? (percent chance)
         if (rand() % 100 < CHANCE_TO_GENERATE_PACKET)
         {
            // Prepare new packet
            *outgoing = new Packet();
            (*outgoing)->type = NODE_INFORMATION_PACKET;
            (*outgoing)->energyLevels[0] = energyLevel;

            for (int i = 0; i < NUM_NODES; i++)
            {
               (*outgoing)->routingPath[i] = routingPath[i];
            }

            PACKET_COUNTER++;
            (*outgoing)->ID = PACKET_COUNTER;

            // Logging
            cout << "Node " << name << " generated info packet [ "<< (*outgoing)->ID << " ] and sent it to node " << (*outgoing)->routingPath[(*outgoing)->nextDestinationIndex] << endl;
         }

      }

      // If sending a packet now, set state to transmitting
      if (*outgoing != NULL)
      {
         // Lower energy levels for transmitting
         energyLevel -= TRANSMIT_COST;

         // Set transmitting state
         state = TRANSMITTING;
      }
      // Else, set state to idle
      else
      {
         // Lower energy levels for idling
         energyLevel -= IDLE_COST;

         // Set idling state
         state = IDLING;

         // Logging
         cout << "Node " << name << " idling" << endl;
      }
   }

   // return state
   return state;
}


//  PACKET  //////////////////////////////////////////////////////
Packet::Packet()
{
   // routing array pointer is set by node, energylevels array is new for each packet
   energyLevels = new double[NUM_NODES];
   routingPath = new string[NUM_NODES];

   nextDestinationIndex = 1;
}

Packet& Packet::operator=(const Packet& other)
{
   energyLevels = new double[NUM_NODES];
   routingPath = new string[NUM_NODES];

   for (int i = 0; i < NUM_NODES; i++)
   {
      energyLevels[i] = other.energyLevels[i];
   }

   for (int i = 0; i < NUM_NODES; i++)
   {
      routingPath[i] = other.routingPath[i];
   }

   type = other.type;
   ID = other.ID;
   information = other.information;
   nextDestinationIndex = other.nextDestinationIndex;

   // return this
   return *this;
}