/***************************************************************/
/* PROGRAM:      Energy Constrained Relay Network Simulator    */
/*                                                             */
/* AUTHORS:      Connor Parkinson, Liam Gomez                  */
/*                                                             */
/* DATE:         12/05/2016                                    */
/*                                                             */
/* PURPOSE:      CPE 400 Project - Simulator of a network for  */
/*               transmitting disaster area information to a   */
/*               command center using an energy-constrained    */
/*               relay network                                 */
/*                                                             */
/***************************************************************/

//  Header Files  //////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <array>
#include <time.h>
#include <ctime>
#include "Classes.h"
#include "Simulator.h"
#include "list/ListLinked.h"
using namespace std;

//  Function Prototypes  ///////////////////////////////////////////////////////


//  Main Function  /////////////////////////////////////////////////////////////
int main()
{
    // Create network of nodes
    Simulator simulation;

    // Run simulation
    simulation.run();

    return 0;
}

//  Supporting Function Implementation  ////////////////////////////////////////
