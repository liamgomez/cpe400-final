#include "Graph.h"

Graph::Graph()
{

}

Graph::Graph(const string names[], const int size)
{
    // initialize all vert labels, and edge weights
    for (int i = 0; i < size; ++i)
    {
        for (int j = 0; j < size; ++j)
        {
            edgeMatrix[names[i]][names[j]] = numeric_limits<double>::max();
        }
    }
}

Graph::~Graph()
{

}


bool Graph::insertEdge(const string &start, const string &end, double weight)
{
    if (exists(start, end))
    {
        edgeMatrix[start][end] = weight;
        return true;
    }
    else
    {
        cout << "ERROR : BAD KEY " << endl;
        return false;
    }
}

double Graph::getWeight(const string &start, const string &end)
{
    if (exists(start, end))
    {
        return edgeMatrix[start][end];
    }
    else
        return -1.0;
}

bool Graph::exists(const string& start, const string& end)
{
    return (edgeMatrix.find(start) != edgeMatrix.end()) && (edgeMatrix[start].find(end) != edgeMatrix[start].end());
}

bool Graph::hasEdgeTo(const string &vertex1, const string &vertex2)
{
    // ensure the map keys exist
    if (exists(vertex1, vertex2))
    {
        double edgeValue = edgeMatrix[vertex1][vertex2];
        return (edgeValue != numeric_limits<double>::max()) && (edgeValue > 0.0);
    }
    else
        return false;
}

void Graph::printMatrix()
{
    // note this is printed out of order, but does not change the accuracy of the matrix
    cout << "WEIGHTED GRAPH (infinite weight is '-'): " << endl << endl;

    for (map<string, map<string, double>>::iterator it = edgeMatrix.begin(); it != edgeMatrix.end(); ++it)
        cout << "\t" << it->first;

    cout << endl;

    for (map<string, map<string, double>>::iterator row = edgeMatrix.begin(); row != edgeMatrix.end(); ++row)
    {
        cout << row->first;

        for (map<string, double>::iterator col = edgeMatrix[row->first].begin(); col != edgeMatrix[row->first].end(); ++col)
        {
            if (col->second != numeric_limits<double>::max())
            {
                cout << "\t" << col->second;
            }
            else
            {
                cout << "\t" << '-';
            }
        }

        cout << endl;
    }
}

vector<string> Graph::findShortestPath(const string &source, const string &dest)
{
    unordered_map<string, double> distances;
    unordered_map<string, string> previous;
    vector<string> nodes;
    vector<string> path;

    auto comparator = [&] (string left, string right) { return distances[left] > distances[right]; };

    for (auto& vertex : edgeMatrix)
    {
        if (vertex.first == source)
        {
            distances[vertex.first] = 0.0;
        }
        else
        {
            distances[vertex.first] = numeric_limits<double>::max();
        }
        
        nodes.push_back(vertex.first);
        push_heap(begin(nodes), end(nodes), comparator);
    }

    while (!nodes.empty())
    {
        pop_heap(begin(nodes), end(nodes), comparator);
        string smallest = nodes.back();
        nodes.pop_back();
        
        if (smallest == dest)
        {
            while (previous.find(smallest) != end(previous))
            {
                path.push_back(smallest);
                smallest = previous[smallest];
            }

            break;
        }
        
        if (distances[smallest] == numeric_limits<double>::max())
        {
            break;
        }
        
        for (auto& neighbor : edgeMatrix[smallest])
        {
            double alt = distances[smallest] + neighbor.second;
            if (alt < distances[neighbor.first])
            {
                distances[neighbor.first] = alt;
                previous[neighbor.first] = smallest;
                make_heap(begin(nodes), end(nodes), comparator);
            }
        }
    }

    // print the path for analysis
    cout << "Routing path for " << source << " --> " << dest << " : " << source << " --> ";

    for (vector<string>::reverse_iterator i = path.rbegin(); i != path.rend(); ++i)
    {
        cout << *i;
        if (i + 1 != path.rend())
        {
            cout << " --> ";
        }
    }

    cout << endl;

    return path;
}
