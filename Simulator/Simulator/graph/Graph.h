// Header Files ////////////////////////////////////////////////////////////////
#ifndef MAP_GRAPH
#define MAP_GRAPH

#include <map>
#include <iostream> 
#include <string>
#include <vector>
#include <unordered_map>
#include <limits>
#include <algorithm>
using namespace std;

// Class Spec //////////////////////////////////////////////////////////////////
class Graph
{
    public:
        // Default constructor
        Graph();

        // initializes all map[name][name] to infinite edge
        Graph(const string names[], const int size);

        // Destructor
        ~Graph();

        // Inserts an edge between two given nodes, updates edge if its exists
        bool insertEdge(const string &start, const string &end, double weight);

        // gets the weight for an edge
        double getWeight(const string &start, const string &end);

        // verifies that the keys for the map exists (ie. node names)
        bool exists(const string &start, const string &end);

        // check if an edge is set (ie. not infinite)
        bool hasEdgeTo(const string &vertex1, const string &vertex2);

        // prints the matrix for debugging
        void printMatrix();

        // find the shortest path between two nodes and returns the path as a vector
        vector<string> findShortestPath(const string &source, const string &dest);

    private:
        map<string, map<string, double>> edgeMatrix;
};

#include "Graph.cpp"
#endif