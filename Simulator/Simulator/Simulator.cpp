#include "Simulator.h"
#include "list/ListLinked.cpp"

Simulator::Simulator()
{
    // Figure out the names of nodes to populate graph with
    string names[NUM_NODES + 1];

    for (int i = 0; i < NUM_NODES; ++i)
    {
        names[i] = std::to_string(i + 1);
    }

    // init central command
    centralComand.name = "C";
    centralComand.xLoc = X_COMMAND_CENTER;
    centralComand.yLoc = Y_COMMAND_CENTER;
    centralComand.energyLevel = -1.0;
    names[NUM_NODES] = centralComand.name;

    // Initialize the weighted graph with the node names and inf edges
    networkGraph = new Graph(names, NUM_NODES + 1);

    // insert edge for cc
    networkGraph->insertEdge(centralComand.name, centralComand.name, 0);

    // give the nodes an initial position
    initNodes();

    // populate the weighted graph
    populateGraph();
}

Simulator::~Simulator()
{

}

void Simulator::initNodes()
{
    // Loop through nodes
    for( int i = 0; i < NUM_NODES; i++ )
    {
        // Set node name
        members[i].name = std::to_string(i + 1);

        // Generate random X Y location
        members[i].xLoc = rand() % X_MAX;
        members[i].yLoc = rand() % Y_MAX;

        // Set starting energy level
        members[i].energyLevel = FULL_ENERGY;

        // Set default state
        members[i].state = IDLING;

        // output to console
        cout << "Initializied node " << members[i].name << " - X: " << members[i].xLoc << " Y: " << members[i].yLoc << endl;
    }
}

void Simulator::populateGraph()
{
    // Loop through every node compare with distance
    for (int popIndex = 0; popIndex < NUM_NODES; ++popIndex)
    {
        // for every node we need to determine what connections it can have
        for (int neighborIndex = 0; neighborIndex < NUM_NODES; ++neighborIndex)
        {
            // Dont add edge for itself (handled in graph)
            if (popIndex != neighborIndex)
            {
                double realDist = calculateNodeDistance(members[popIndex], members[neighborIndex]);

                // if its less than max transition distance then we can add an edge
                if (realDist <= MAX_TRANS_DIST)
                {
                    // insert the edge into the graph with distance as weight
                    networkGraph->insertEdge(members[popIndex].name, members[neighborIndex].name, realDist);

                    // Print it
                    cout << "Found connection - " << members[popIndex].name << "  ---->  "
                         << members[neighborIndex].name << "    Distance: " << realDist << endl;
                }
            }
            else
            {
                networkGraph->insertEdge(members[popIndex].name, members[neighborIndex].name, 0);
            }
        }

        // Get the nodes distance from central commmand
        double distCom = calculateNodeDistance(members[popIndex], centralComand);

        // if its less than max transition distance then we can add an edge
        if (distCom <= MAX_TRANS_DIST)
        {
            networkGraph->insertEdge(members[popIndex].name, centralComand.name, distCom);
            networkGraph->insertEdge(centralComand.name, members[popIndex].name, distCom);
        }
    }

    // Debugging only, prints the adj. matrix for weighted graph
    // networkGraph->printMatrix();
}

void Simulator::run()
{
    // Calculate initial graph weights and paths for nodes
    updatePacketRoutes();

    // initialize event queue
    List<Packet> EventQueue;

   // initialize array of known initial energy levels at command center
   int energyLevelsAndAge[NUM_NODES][2];
   for (int arrIndex = 0; arrIndex < NUM_NODES; arrIndex++)
   {
      energyLevelsAndAge[arrIndex][0] = FULL_ENERGY;
      energyLevelsAndAge[arrIndex][1] = 0; // num sim interations since last update
   }

   // instantiate energy and temp packets
   Packet * energyPacket = NULL;
   Packet temp;

   // Loop until sim ends
   for (int simIteration = 0; simIteration <= 10; simIteration++)
   {
      // Perform command center operations

         // Calculate new graph weights and paths for nodes, if interval hit
         if ((simIteration + 1) % 4 == 0)
         {
            // Calculate new routes based on energy level and distance
            updateRoutingWeights();

            // update any packet routes by finding shortest path on new weights
            updatePacketRoutes();
         }

         // Increment energyLevels age and send energy update request to nodes who have not transmitted in a while
         for (int arrIndex = 0; arrIndex < NUM_NODES; arrIndex++)
         {
            energyLevelsAndAge[arrIndex][1]++; // num sim interations since last update

            // If max age reached
            if (energyLevelsAndAge[arrIndex][1] > 21)
            {
               energyPacket = new Packet();
               energyPacket->type = CC_ENERGY_REQUEST;

               // Set routing path to name of node the energy update is for
               energyPacket->nextDestinationIndex = 0;
               energyPacket->routingPath[energyPacket->nextDestinationIndex] = arrIndex + 1;

               // Add to event queue
               EventQueue.insert(*energyPacket);

               // Logging
               cout << "Command Center generated an energy request packet for node " << arrIndex + 1 << endl;
            }

         }

         // Check the event queue for messages sent to the Command Center (log and remove)
         // i.e. if packets routingPath at nextDestinationIndex is command center
         // loop through event queue to see if there was a packet sent to this node
         if (!EventQueue.isEmpty())
         {
            // Start at beginning of list
            EventQueue.gotoBeginning();

            // Loop until end of list
            bool continueSearch = true;
            while (continueSearch && !EventQueue.isEmpty())
            {
               // Get packet from list
               temp = EventQueue.getCursor();

               // If packets next destination is the command center
               if (temp.routingPath[temp.nextDestinationIndex] == "Command Center")
               {
                  // Delete packet from queue
                  EventQueue.remove();

                  // logging
                  cout << "Command Center received packet [ " << temp.ID << " ] originating from node " << temp.routingPath[0] << endl;

                  // update energy levels and age
               }
               else
               {
                  // Go to next item
                  continueSearch = EventQueue.gotoNext();
               }
            }
         }

      // Perform event operations
      handleEvents(members, EventQueue);

      // wait for user every 10 iterations
 //     if(simIteration % 3 == 0)
 //        system("PAUSE");
   }
}

void Simulator::handleEvents(Node members[], List<Packet> &EventQueue)
{
   Packet * tempPacketOut = NULL;
   Packet * tempPacketIn = NULL;
   Packet temp;
   int returnState;

   // loop through all nodes
   for (int i = 0; i < NUM_NODES; i++)
   {
      // loop through event queue to see if there was a packet sent to this node
      if (!EventQueue.isEmpty())
      {
         // Start at beginning of list
         EventQueue.gotoBeginning();

         // Loop until end of list or packet found
         bool continueSearch = true;
         while (continueSearch)
         {
            // Get packet from list
            temp = EventQueue.getCursor();
            tempPacketOut = &temp;

            // Should not hit this
            if (tempPacketOut->nextDestinationIndex >= NUM_NODES)
            {
               // Logging
               cout << "Removed packet [ " << tempPacketOut->information << " ] from queue: destination index out of bounds" << endl;

               // Delete packet from queue
               EventQueue.remove();
            }
            // If packets next destination is this node
            else if (tempPacketOut->routingPath[tempPacketOut->nextDestinationIndex] == members[i].name)
            {
               // Delete packet from queue
               EventQueue.remove();

               // Set input packet
               tempPacketIn = tempPacketOut;

               // End search
               continueSearch = false;
            }
            else
            {
               // Go to next item
               tempPacketIn = NULL;
               continueSearch = EventQueue.gotoNext();
            }
         }
      }

      tempPacketOut = NULL;
      Packet ** newPacket = &tempPacketOut;
      // Call node event handler
      returnState = members[i].doEvents(tempPacketIn, newPacket);

      // if node transmitted
      if (returnState == TRANSMITTING)
      {
         // add packet to event handling list
         EventQueue.gotoEnd();
         EventQueue.insert(*tempPacketOut);
      }
      // if node was charging and was sent a packet
      else if (returnState == CHARGING && tempPacketIn != NULL)
      {
         // add packet back into queue
         EventQueue.insert(*tempPacketIn);
      }
   }
}

void Simulator::updatePacketRoutes()
{
    // variables
    int routeIndex = 0;
    cout << endl << endl;

    // names should probably come from node array yolo
    for (int i = 0; i < NUM_NODES; ++i)
    {
        vector<string> path = networkGraph->findShortestPath(std::to_string(i +1), "C");
        string *nodeRoutingPath = members[i].routingPath;

        if (path.size() <= 0)
        {
            // nodes routing path always starts with their own name
            nodeRoutingPath[0] = "-2";
        }
        else
        {
            nodeRoutingPath[0] = members[i].name;
            routeIndex++;
        }

        // loop through rest of path
        for (vector<string>::reverse_iterator pit = path.rbegin(); pit != path.rend(); ++pit)
        {
            if (*pit == centralComand.name)
            {
                nodeRoutingPath[routeIndex] = "Command Center";
                routeIndex++;
            }
            else
            {
                nodeRoutingPath[routeIndex] = *pit;
                routeIndex++;
            }
        }

        nodeRoutingPath = nullptr;
        routeIndex = 0;
    }

    cout << endl << endl;
}

void Simulator::updateRoutingWeights()
{
    // Loop through the nodes and determine which ones need weights adjusted
    // to avoid excessive battery loss
    for (int updateIndex = 0; updateIndex < NUM_NODES; ++updateIndex)
    {
        for (int edgeIndex = 0; edgeIndex < NUM_NODES; ++edgeIndex)
        {
            if (updateIndex != edgeIndex)
            {
                // calculate node distanc 
                double dist = calculateNodeDistance(members[updateIndex], members[edgeIndex]);

                // ensure an edge previously existed
                if (networkGraph->hasEdgeTo(members[updateIndex].name, members[edgeIndex].name) && dist <= MAX_TRANS_DIST)
                {
                    double weight = calcWeight(dist, members[edgeIndex].energyLevel);
                    networkGraph->insertEdge(members[updateIndex].name, members[edgeIndex].name, weight);
                }
            }
        }
    }
}

double Simulator::calcWeight(double dist, double batteryLevel)
{
    // Edge scaling = distance + bat_scalar * bat_level

    // calculate how much battery the node has lost
    double batteryLost = 100.0 - batteryLevel;

    return (dist + (BATTERY_WEIGHT_FACTOR * batteryLost));
}

double Simulator::calculateNodeDistance(const Node &first, const Node &second)
{
    // get x dist
    double difX = first.xLoc - second.xLoc;

    // get y dist
    double difY = first.yLoc - second.yLoc;

    // calculate actual distance
    return sqrt(pow(difX, 2.0) + pow(difY, 2.0));
}
