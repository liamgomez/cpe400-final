// Include Files ///////////////////////////////////////////////////////////////
#ifndef SIMULATOR
#define SIMULATOR

#include <iostream>
#include <string>
#include <cstdlib> // for abs
#include <cmath> // sqrt / pow
#include <map> // edge matrix
#include <vector>
using namespace std;

#include "Classes.h"
#include "list/ListLinked.h"
#include "graph/Graph.h"

// Global Variables ////////////////////////////////////////////////////////////
const double MAX_TRANS_DIST = 30.0;
const double BATTERY_WEIGHT_FACTOR = 0.05;

// Class Spec //////////////////////////////////////////////////////////////////
class Simulator
{
    public:
        Simulator();
        ~Simulator();

        void initNodes();
        void populateGraph();
        void handleEvents(Node members[], List<Packet> &EventQueue);
        void updatePacketRoutes();
        void updateRoutingWeights();
        double calcWeight(double dist, double batteryLevel);
        double calculateNodeDistance(const Node &first, const Node &second);
        void run();

        // Data members
        Node members[NUM_NODES];
        Node centralComand;
        Graph *networkGraph;
};

#include "Simulator.cpp"
#endif
