#pragma once
#include <string>

using namespace std;

//  Global Constants  //////////////////////////////////////////////////////////

   // Simulation constants
   const int NUM_NODES = 30; // number of nodes to generate

   const int X_MAX = 100; // max x of graph
   const int Y_MAX = 100; // max y of graph

   const int X_COMMAND_CENTER = 50;
   const int Y_COMMAND_CENTER = 50;
   const string NAME_COMMAND_CENTER = "CC";

   const int CHANCE_TO_GENERATE_PACKET = 20;

   // Node energy constants
   const double FULL_ENERGY = 100;
   const double CHARGE_CYCLE = 20;
   const double RECEIVE_COST = 10;
   const double TRANSMIT_COST = 12;
   const double IDLE_COST = 5;
   const double RECHARGE_LEVEL = 20;

   // Node state constants
   const int IDLING = 1;
   const int TRANSMITTING = 2;
   const int CHARGING = 3;

   // Packet types
   const int CC_UPDATE = 1;
   const int CC_ENERGY_REQUEST = 2;
   const int NODE_INFORMATION_PACKET = 3;


//  Class Prototypes  //////////////////////////////////////////////////////////
class Node;
class Packet;

//  Class Headers //////////////////////////////////////////////////////////////
class Node
{
   public:
      string name;
      int xLoc, yLoc;
      int state;
      double energyLevel;
      string * routingPath;

      Node();
      int doEvents(Packet * incoming, Packet ** outgoing); //(performs operation in node for one iteration)
   private:
};

class Packet
{
   public:
      int ID;
      int type;
      string information;
      double * energyLevels; // energy of each node in the path
      string * routingPath;
      int nextDestinationIndex;

      Packet();

      Packet& operator=(const Packet& other);
   private:
};

#include "Classes.cpp"
